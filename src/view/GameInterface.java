package view;

import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

import controller.TurnInterface;
import model.deck.Deck;
import model.player.Player;

public interface GameInterface {
	/**
	 * Method that log events of the game
	 * @param log
	 */
	void logger(String log);
	/**
	 * Method that disable the play tha lose all life points
	 * @param i 
	 * @param i
	 */
	void dead(int morto, int cambio, int i);
	/**
	 * Method that end a turn and modifies the reference of currentPlayer and
	 * otherPlayer.
	 */
	void fineTurno();

	/**
	 * Method that buys a card and replaces that card whith another one
	 * @param indiceCarte TODO
	 */
	void buyCard(Integer indiceCarte);

	/**
	 * Solve dices once rolled
	 * @param players TODO
	 */
	void solveDices(List<Player> players);
	/**
	 * Kings'damages
	 * @param players TODO
	 * @param currentPlayer TODO
	 */
	void dmgKing(List<Player> players, Player currentPlayer);
	/**
	 * Method that does the last functions of dice solution
	 * @param players TODO
	 * @param currentPlayer TODO
	 */
	void endSolve(List<Player> players, Player currentPlayer);
	/**
	 * Method that set the btn for the info
	 * @param desc TODO
	 */
	void gameInfo(String desc);

	/**
	 * Method that set the btn for the info
	 */
	void setInfo(String desc);
	/**
	 * Set the button of the cards on the game table.
	 * @param mazzo TODO
		 */
	void setFieldCards(Deck mazzo);
	/**
	 * This method is about the change way of the cards in the field
	 * @param players TODO
	 * @param player TODO
	 * @param otherPlayers TODO
	 */
	void actionCard(Deck mazzo, List<Player> players, Player player);
	/**
	 * 
	 * @param results
	 */
	void refreshDices(List<Integer> results);
	
	void refreshPoints(int i);
	int vittoria(String vincitore);
	
	int getWinChoice();

	}
