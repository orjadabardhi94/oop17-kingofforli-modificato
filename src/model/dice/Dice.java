package model.dice;

import java.util.Random;
/**
 *
 *
 */
public class Dice {

    private static final int FACES = 6;

    private int number;
    private boolean unlock;
    
    
    /**
     * Dice constructor.
     */
    public Dice() {
        this.rollDice();
        this.unlock=true;//sbloccato
    }
    /**
     * 
     * @return result of the roll.
     */
    public int getNumber() {
        return number;
    }
    /**
     * Change value of the dice.
     */
    public final void rollDice() {
        this.number = new Random().nextInt(FACES) + 1;
    }
    /**
     * modifies the state from locked to unlocked
     */
    public void changeState() {
    	this.unlock ^= true;
    }
    
    public boolean isUnlocked() {
    	return this.unlock;
    }
}
