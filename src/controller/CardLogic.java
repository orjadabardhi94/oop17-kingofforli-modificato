package controller;

import java.util.List;
import model.deck.Deck;
import model.player.Player;
import view.GameInterface;

public interface CardLogic {

	/**
	 * Set the button of the cards on the game table.
	 * @param g TODO
	 */
	void setFieldCards(GameInterface g);

	/**
	 * Method that set the btn for the info
	 */
	void setInfo();

	/**
	 * This method calculates if the player has enough money to buy a card
	 * @param currentPlayer TODO
	 * @param g TODO
	 */

	void canBuyCard(Player currentPlayer, GameInterface g);

	/**
	 * This method is about the change way of the cards in the field
	 * @param t TODO
	 * @param index TODO
	 * @param g TODO
	 * @param otherPlayers TODO
	 * @return TODO
	 */
	boolean actionBtn(TurnInterface t, int index, GameInterface g);
	 /**
	  * Refreshes players after every turn
	  */
	 void refreshPlayer(Player currentPlayer, List<Player> players);
	 /**
	  * Gets the description of card
	  */
	 String getDescription();
	 /**
	  * Getter of Deck
	  */
	 Deck getDeck();
}
