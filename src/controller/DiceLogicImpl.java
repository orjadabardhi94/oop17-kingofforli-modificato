package controller;

import java.util.ArrayList;
import java.util.List;
import model.dice.Dice;
import model.player.Player;
import view.GameInterface;

public class DiceLogicImpl implements DiceLogic {
	private boolean roll; // controllo se un giocatore ha lanciato almeno una volta i dadi
	private final List<Dice> realDices;// dadi reali
	private int lanci;// contatore numero lanci dai dadi
	private boolean endDice;

	public DiceLogicImpl() {
		this.roll = false;
		this.endDice = false;
		this.lanci = 0;
		this.realDices = new ArrayList<>();
	}

	@Override
	public boolean isRoll() {
		return roll;
	}

	@Override
	public void setDices() {
		for (int i = 0; i < 6; i++) {
			realDices.add(new Dice());
		}
	}

	@Override
	public List<Integer> rollDice() {
		List<Integer> results=new ArrayList<>();
		if (lanci <= 2 && !endDice) {
			lanci++;
			roll = true;
			for (int i = 0; i < 6; i++) {
				if (realDices.get(i).isUnlocked()) {
					realDices.get(i).rollDice();
				}
				results.add(realDices.get(i).getNumber());
			}
		}
		return results;
	}

	@Override
	public void solveDice(Player currentPlayer, List<Player> otherPlayer, boolean dmgKingX, List<Player> players,
			TurnInterface t, int reachPoint, GameInterface g) {
		if (endDice == false) {
			int vita = 0;
			int soldi = 0;
			int schiaffi = 0;
			int cont1 = 0;
			int cont2 = 0;
			int cont3 = 0;
			for (int i = 0; i < 6; i++) {
				if (realDices.get(i).getNumber() == 1) {
					cont1++;
				}
				if (realDices.get(i).getNumber() == 2) {
					cont2++;
				}
				if (realDices.get(i).getNumber() == 3) {
					cont3++;
				}
				if (realDices.get(i).getNumber() == 4) {
					schiaffi++;
				}
				if (realDices.get(i).getNumber() == 5) {
					vita++;
				}
				if (realDices.get(i).getNumber() == 6) {
					soldi++;
				}
			}
			/* controllo livelli */
			switch (cont1) {
			case 3:
				currentPlayer.increasePoints(1);
				g.logger(currentPlayer.getName() + " guadagna 1 punto");
				break;
			case 4:
				currentPlayer.increasePoints(2);
				g.logger(currentPlayer.getName() + " guadagna 2 punti");
				break;
			case 5:
				currentPlayer.increasePoints(3);
				g.logger(currentPlayer.getName() + " guadagna 3 punti");
				break;
			case 6:
				currentPlayer.increasePoints(4);
				g.logger(currentPlayer.getName() + " guadagna 4 punti");
				break;
			default:
				currentPlayer.increasePoints(0);
				break;
			}

			switch (cont2) {
			case 3:
				currentPlayer.increasePoints(2);
				g.logger(currentPlayer.getName() + " guadagna 2 punti");
				break;
			case 4:
				currentPlayer.increasePoints(3);
				g.logger(currentPlayer.getName() + " guadagna 3 punti");
				break;
			case 5:
				currentPlayer.increasePoints(4);
				g.logger(currentPlayer.getName() + " guadagna 4 punti");
				break;
			case 6:
				currentPlayer.increasePoints(5);
				g.logger(currentPlayer.getName() + " guadagna 5 punti");
				break;
			default:
				currentPlayer.increasePoints(0);
				break;
			}

			switch (cont3) {
			case 3:
				currentPlayer.increasePoints(3);
				g.logger(currentPlayer.getName() + " guadagna 3 punti");
				break;
			case 4:
				currentPlayer.increasePoints(4);
				g.logger(currentPlayer.getName() + " guadagna 4 punti");
				break;
			case 5:
				currentPlayer.increasePoints(5);
				g.logger(currentPlayer.getName() + " guadagna 5 punti");
				break;
			case 6:
				currentPlayer.increasePoints(6);
				g.logger(currentPlayer.getName() + " guadagna 6 punti");
				break;
			default:
				currentPlayer.increasePoints(0);
				break;
			}
			/* controllo livelli */

			if (currentPlayer.isKing()) {
				int temp = schiaffi;
				otherPlayer.forEach(p -> {
					p.takeDamages(temp);
				});
			g.logger(currentPlayer.getName() + " da " + schiaffi + " schiaffi agli altri giocatori");
			} else {
				for (int i = 0; i < otherPlayer.size(); i++) {
					if (otherPlayer.get(i).isKing()) {
						otherPlayer.get(i).takeDamages(schiaffi);
						g.logger(otherPlayer.get(i).getName() + " prende " + schiaffi + " schiaffi da "
								+ currentPlayer.getName());
					}
				}
			}

			if (!currentPlayer.isKing() && schiaffi > 0) {
				dmgKingX = true;
			}

			if (soldi != 0) {
				currentPlayer.earnMoney(soldi);
			}
			g.logger(currentPlayer.getName() + " ha guadagnato " + soldi + " soldi");
			if (currentPlayer.isKing()) {
				currentPlayer.rechargeLife(0);
			} else {
				currentPlayer.rechargeLife(vita);
				g.logger(currentPlayer.getName() + " ha guadagnato " + vita + " di vita");
			}
			Player k = players.stream().filter(p -> p.isKing()).findAny().get();
			if (dmgKingX == true && k.getLife() > 0) {
				t.dmgKing();
			}
			g.solveDices(players);
			endDice=true;
			lanci=0;
			g.endSolve(players, currentPlayer);
		}
	}

	@Override
	public int getLanci() {
		return this.lanci;
	}

	@Override
	public void setEndDice(boolean b) {
		this.endDice = b;
	}

	@Override
	public void setRoll(boolean b) {
		this.roll=b;
	}

	@Override
	public void resetDice() {
			realDices.forEach( d -> {
			if(!d.isUnlocked()) {
				d.changeState();
			}
		});
	}

	@Override
	public void changeSate(int indDado) {
		realDices.get(indDado).changeState();
	}
}
