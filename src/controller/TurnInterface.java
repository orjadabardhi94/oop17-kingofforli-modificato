package controller;

import java.util.ArrayList;
import java.util.List;
import model.player.Player;
import view.GameInterface;

public interface TurnInterface {

	/**
	 * Set the points that a player has to reach to win the game.
	 * 
	 * @param p
	 */
	void setReachPoint(int p);

	/**
	 * Method that return if a player has rolled at least one time dices.
	 * 
	 * @return
	 */
	boolean getRoll();

	/**
	 * Method to return the reference of the player.
	 * 
	 * @return
	 */
	int getCurrentInt();

	/**
	 * Load the dice of the game table.
	 */
	void setDices();

	/**
	 * Set the button of the cards on the game table.
	 */
	void setFieldCards();

	/**
	 * Method that set the btn for the info
	 */
	void setInfo();

	/**
	 * Method to roll dice.
	 * @return TODO
	 */
	List<Integer> rollDice();

	/**
	 * Method to solve the results of dice.
	 */
	void solveDice();

	/**
	 * Method that end a turn and modifie the reference of currentPlayer and
	 * otherPlayer.
	 */
	void fineTurno();

	/**
	 * JOptionPane that came out if a player win the game.
	 * 
	 * @param vincitore
	 */
	void vittoria(Player vincitore);

	/**
	 * Creation of the players list.
	 * 
	 * @param names
	 * @param life
	 * @param cash
	 */
	void createPlayers(ArrayList<String> names, int life, int cash);

	/**
	 * Getter for the number of the players.
	 * 
	 * @return
	 */
	int getNPlayer();

	/**
	 * Method that set the max life of the players. You con't go over this number.
	 * 
	 * @param maxLife
	 */
	void setMaxLife(int maxLife);

	/**
	 * Method that set the role of the king.
	 * 
	 * @param k
	 */
	void setKing(int k);

	/**
	 * Getter of the maxLife of the player.
	 * 
	 * @return
	 */
	int getMaxLife();

	/**
	 * Getter of the startMoney
	 * 
	 * @return
	 */
	int getStartMoney();

	/**
	 * Getter of the maxPoint
	 * 
	 * @return
	 */
	int getMaxPoint();

	/**
	 * This method allows the exchange from king to normalplayer if needed
	 * 
	 */
	void dmgKing();
	
	/**
	 * This method calculates if the player has enough money to buy a card
	 */
	void canBuyCard();

	/**
	 * This method is about the change way of the cards in the field
	 * @return TODO
	 */
	boolean actionBtn(int i);
	
	/**
	 * This method manages the death of all players
	 */
	void morte(int i);
	
	/**
	 * blocca sblocca dadi
	 */
	void lUnlock(int indDado);
	
	/**
	 * method for kings damage
	 */
	void changeKing(int choice);
	
	/**
	 * Sets the gui 
	 */
	void setGUI(GameInterface g);
	
	/**
	 * String that describes the card
	 * @param indCard
	 * @return
	 */
    String cardTooltip(int indCard);
    
    /**
     * manages when a card is clicked
     */
	void setCardListener();
	
	/**
	 * returns the cards that can be buyed
	 * @return
	 */
    int currentAvailability();
    
    /**
     * returns the life of a player
     * @param i
     * @return
     */
    int pLife(int i);
    
    /**
     * returns the money of a player
     * @param i
     * @return
     */
    int pMoney(int i);
    
    /**
     * returns the points of a player
     * @param i
     * @return
     */
    int pPoints(int i);
    
    /**
     * Tells if a card can be buyed
     * @param i
     * @return
     */
    boolean isCardAviable(int i);
    
    /**
     * returns the name of the card that was picked
     * @param i
     * @return
     */
    String cardPickedName(int i);
    
    /**
     * Gets the index of currentPlayer
     * @return
     */
	int getIndex();

}
