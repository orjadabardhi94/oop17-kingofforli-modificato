package controller;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import view.*;
import model.player.*;

public class Turn extends TestForm implements TurnInterface{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean dmgKingX = false;
	private int startLife;
	private int startCash;
	private int reachPoint;
	private List<Player> players = new ArrayList<>();
	private List<Player> tmpPlayers = new ArrayList<>();
	private Player currentPlayer;	// il giocatore di turno
	private final List<Player> otherPlayer = new ArrayList<>();//lista di giocatori che invece non sono di turno
	private boolean canEndTurn = false;	// possibilit� di finire il turno
	private int index;
	private final DiceLogic dl;// creato oggetto di tipo DiceLogic
	private final CardLogicImpl cl;
	private GameInterface g;
	
	/**
	 * Constructor of the class Turn.
	 * 
	 * @param n
	 * @param l
	 * @param c
	 * @param p
	 */
	public Turn(Integer n, Integer l, Integer c, Integer p) {
		dl = new DiceLogicImpl();
		cl = new CardLogicImpl();
		this.startLife = l.intValue();
		this.startCash = c.intValue();
		this.reachPoint = p.intValue();
	}
	
	@Override
	public  void setReachPoint(final int p) {
		reachPoint = p;
	}
	
	@Override
	public  boolean getRoll() {
		return dl.isRoll();
	}
	
	@Override
	public  int getCurrentInt() {
		return tmpPlayers.indexOf(currentPlayer);
	}

	@Override
	public  void setDices() {
		dl.setDices();
		g.logger("Dadi caricati");
	}
	
	@Override
	public  void setFieldCards() {
		cl.setFieldCards(g);
		cl.refreshPlayer(currentPlayer, players);
	}
	
	@Override
	public  void setInfo() {
		cl.setInfo();
		g.setInfo(cl.getDescription());
	}

	@Override
	public List<Integer> rollDice() {
		List<Integer> results = dl.rollDice();
		System.out.println(currentPlayer.getMoney());
		if (dl.getLanci() == 3) {
			g.refreshDices(results);
			dl.solveDice(currentPlayer, otherPlayer, dmgKingX, players, this, reachPoint, g);
			canEndTurn=true;
			dl.setEndDice(true);
		}
		return results;
	}

	@Override
	public void solveDice() {
		dl.solveDice(currentPlayer, otherPlayer, dmgKingX, players, this, reachPoint, g);
		canEndTurn = true;
		dmgKingX = false;
	}
	
	@Override
	public void fineTurno() {
		if (canEndTurn == false) {
			g.logger("CONFERMARE LANCIO DADI");
		} else {
			dl.setRoll(false);
			g.logger(currentPlayer.getName() + " FINISCE IL TURNO");
			dl.resetDice();
			index++;
			if (index >= players.size()) {
				index = 0;
			}
			currentPlayer = players.get(index);
			if (currentPlayer.isKing()) {
				currentPlayer.increasePoints(2);
				
			}
			otherPlayer.clear();
			for (int i = 0; i < players.size(); i++) {
				if (players.get(i) != currentPlayer) {
					otherPlayer.add(players.get(i));
				}
			}
			cl.refreshPlayer(currentPlayer, players);
			g.fineTurno();
			g.logger(currentPlayer.getName() + " INIZIO TURNO");
			dl.setEndDice(false);
			canEndTurn = false;
			players.forEach(p -> {
				if (p.getPoints() >= reachPoint || players.size() == 1) {
					vittoria(currentPlayer);
				}
			});
		}
	}
	
	@Override
	public void vittoria(Player vincitore) {
		// interpret the user's choice
		if (g.vittoria(vincitore.getName()) == g.getWinChoice()) {
			System.exit(0);
		}
	}
	
	@Override
	public void createPlayers(ArrayList<String> names, int life, int cash) {
		names.forEach(n -> players.add(new PlayerImpl(n, life, cash)));
		tmpPlayers.addAll(players);
	}
	
	@Override
	public int getNPlayer() {
		return players.size();
	}

	@Override
	public void setMaxLife(int maxLife) {
		startLife = maxLife;
	}
	
	@Override
	public void setKing(int k) {
		index = k;
		players.get(k).changeRule();
		players.get(k).increasePoints(1);
		currentPlayer = players.get(k);
		otherPlayer.addAll(players.stream().filter(p->!p.isKing()).collect(Collectors.toList()));//aggiunge ad otherplayer tutti i giocatori che non sono king
		g.solveDices(players);
		g.logger(players.get(k).getName() + " � il King di Forl�!");
		g.logger(players.get(k).getName() + " INIZIO TURNO");
	}
	
	@Override
	public int getMaxLife() {
		return startLife;
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Home window = new Home();
					window.frameHome.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public void dmgKing() throws IllegalStateException {
		g.dmgKing(players, currentPlayer);
	}

	@Override
	public void canBuyCard() {
		cl.canBuyCard(currentPlayer, g);
	}

	@Override
	public boolean actionBtn(int i) {
		return cl.actionBtn(this, i, g);
	}

	@Override
	public void morte(int i) {
		if (i <= index) {
			index--;
		}
		g.logger(players.get(i).getName() + " � morto!");
		int dead = tmpPlayers.indexOf(players.get(i));
		players.remove(i);
		if (!currentPlayer.isKing()) {
			currentPlayer.changeRule();
		}
		g.dead(dead, index, i);
	}

	@Override
	public int getStartMoney() {
		return this.startCash;
	}

	@Override
	public int getMaxPoint() {
		return this.reachPoint;
	}

	@Override
	public void lUnlock(int indDado) {
		dl.changeSate(indDado);
	}

	@Override
	public void changeKing(int choice) {
		players.forEach(p -> {
			if (p.isKing()) {
				p.changeRule();
			}
		});
		currentPlayer.changeRule();
		currentPlayer.increasePoints(1);
	}

	@Override
	public void setGUI(GameInterface g) {
		this.g=g;
	}

	@Override
	public String cardTooltip(int indCard) {
	    return cl.getDeck().showFieldCards().get(indCard).getName() + " - "
                    + cl.getDeck().showFieldCards().get(indCard).getPrice() + " - "
                    + cl.getDeck().showFieldCards().get(indCard).getDescription();
	}
	
	@Override
	public int currentAvailability() {
	    return currentPlayer.getMoneyAndDiscount();
	}
	
	@Override
	public int pLife(int i) {
	    return this.players.get(i).getLife();
	}
	
	@Override
	public int pMoney(int i) {
	    return this.players.get(i).getMoney();
	}
	
	@Override
	public int pPoints(int i) {
	    return this.players.get(i).getPoints();
	}
	
	@Override
	public boolean isCardAviable(int i) {
	    return this.currentPlayer.getMoneyAndDiscount() >= cl.getDeck().showFieldCards().get(i).getPrice();
	}
	
	@Override
	public String cardPickedName(int i) {
	    return cl.getDeck().showFieldCards().get(i).getName();
	}
	
	@Override
	public void setCardListener() {
	    g.actionCard(cl.getDeck(), players, currentPlayer);
	}
	
	@Override
	public int getIndex() {
		return this.index;
	}
}
