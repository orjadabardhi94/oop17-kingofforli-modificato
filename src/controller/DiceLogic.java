package controller;

import java.util.List;
import model.player.Player;
import view.GameInterface;

public interface DiceLogic {
	/**
	 * Method that return if a player has rolled at least one time dices.
	 * 
	 * @return
	 */
	boolean isRoll();

	/**
	 * Load the dice of the game table.
	 */
	void setDices();

	/**
	 * Method to roll dice.
	 * @return TODO
	 */
	List<Integer> rollDice();

	/**
	 * Method to solve the results of dice.
	 * @param currentPlayer TODO
	 * @param otherPlayer TODO
	 * @param dmgKingX TODO
	 * @param players TODO
	 * @param t TODO
	 * @param reachPoint TODO
	 * @param g TODO
	 */
	void solveDice(Player currentPlayer, List<Player> otherPlayer, boolean dmgKingX, List<Player> players, TurnInterface t, int reachPoint, GameInterface g);
	
	/**
	 * Returns number of rolls
	 * @return
	 */
	int getLanci();
	
	/**
	 * rolls finished
	 */
	
	void setEndDice(boolean b);
	/**
	 * Sets the end of roll number
	 */
	
	void setRoll(boolean b);
	
	/**
	 * Resets the dices enabled at end of turn
	 */
	void resetDice();
	
	/**
	 * changes the state of the dice
	 */
	void changeSate(int indDado);
	
}
