package controller;

import java.util.ArrayList;
import java.util.List;
import model.deck.Deck;
import model.deck.DeckImpl;
import model.player.Player;
import view.GameInterface;

public class CardLogicImpl implements CardLogic {

	private final Deck mazzo;
	private final List<String> carteDesc;
	private String desc;
	private int indiceCarte;
	private List<Player> players;
	private final List<Player> otherPlayer;
	private Player currentPlayer;	// il giocatore di turno
	
	public CardLogicImpl() {
		this.mazzo = new DeckImpl();
		this.carteDesc = new ArrayList<>();
		this.otherPlayer = new ArrayList<>();
		this.players = new ArrayList<>();
		this.indiceCarte = 0;
		this.desc="";
	}
	
	@Override
	public void setFieldCards(GameInterface g) {
		mazzo.createDeck();
		mazzo.showFieldCards().forEach(e-> {
			g.logger("Inserita carta "+e.getName()+" al prezzo di "+e.getPrice());
		});
		g.setFieldCards(mazzo);
	}

	@Override
	public void setInfo() {
		mazzo.getDeckCards().forEach(c -> {
			carteDesc.add(c.getName()+" - "+c.getPrice()+" - "+c.getDescription());
		});
		carteDesc.forEach(d -> desc += d + "\n");
	}

	@Override
	public void canBuyCard(Player currentPlayer, GameInterface g) {
			mazzo.showFieldCards().forEach(c -> {
			if (currentPlayer.getMoneyAndDiscount() >= c.getPrice()) {
				g.buyCard(indiceCarte);
			}
			indiceCarte++;
		});
		indiceCarte = 0;
	}

	@Override
	public boolean actionBtn(TurnInterface t, int index, GameInterface g) {
		  final boolean isSingleUse = mazzo.showFieldCards().get(index).isSingleUse();
          currentPlayer.pickCard(mazzo.showFieldCards().get(index), otherPlayer);
          g.logger(currentPlayer.getName() + " usa " + mazzo.showFieldCards().get(index).getName());
          g.logger(mazzo.showFieldCards().get(index).getDescription());
                   mazzo.addCard(index);
          g.logger("Aggiunta al campo carta " + mazzo.showFieldCards().get(index).getName() + " - costo "
                  + mazzo.showFieldCards().get(index).getPrice());
          for (int i = 0; i < players.size(); i++) {
              if (players.get(i).getLife() <= 0) {
                  t.morte(i);
              }
          }
          return isSingleUse;
	}

	@Override
	public void refreshPlayer(Player currentPlayer, List<Player> players) {
		this.currentPlayer = currentPlayer;
		this.players = players;
		this.otherPlayer.clear();
		players.forEach(p -> {
			if(!p.equals(currentPlayer)) {
				otherPlayer.add(p);
			}
		});
	}

	@Override
	public String getDescription() {
		return desc;
	}

	@Override
	public Deck getDeck() {
		return mazzo;
	}

}
